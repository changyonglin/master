package com.ming.producemq;

import com.ming.producemq.config.ProduceImportSelector;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Classname EnableProduceAutoConfigure
 * @Description TODO
 * @Date 2021/1/13 16:18
 * @Created by yanming.fu
 */
@Configuration
@Import(ProduceImportSelector.class)
public class EnableProduceAutoConfigure {
}
