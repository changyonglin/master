package com.ming.consumemq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Classname ImportSelectorConsume
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
@Slf4j
public class ImportSelectorConsume implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        log.info("执行加载mq配置的bean");
        return new String[]{
                "com.ming.consumemq.config.SystemMqConfig",
                "com.ming.consumemq.config.UserMqConfig"
        };
    }
}
