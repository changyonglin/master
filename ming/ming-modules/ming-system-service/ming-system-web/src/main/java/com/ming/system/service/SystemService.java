package com.ming.system.service;

import com.ming.system.mapper.SystemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ming.system.System;
import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * @Classname UserService
 * @Description TODO
 * @Date 2021/1/5 13:59
 * @Created by yanming.fu
 */
@Service
public class SystemService {

    @Autowired
    private SystemMapper systemsMapper;

    public List<System> findSystems() {
        List<System> systems = systemsMapper.selectAll();
        return systems;
    }

    public int insert(){
        System sysNews = System.builder().name("路人甲").id(UUID.randomUUID().toString().replace("-","")).build();
        int insert = systemsMapper.insert(sysNews);
        return insert;

    }



}
