package com.ming.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @Classname UserApplication
 * @Description 用户实体类
 * @Date 2021/1/4 17:47
 * @Created by yanming.fu
 */
@Data
@Builder
@Table(name="sys_user")
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    private String id;
    @Column
    private String name;
    @Column
    private Long age;


}
