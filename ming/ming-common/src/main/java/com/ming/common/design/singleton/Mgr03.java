package com.ming.common.design.singleton;

/**
 * @Classname Mgr02
 * @Description 懒汉式
 * 虽然达到了按需初始化的目的 但会带来线程不安全问题
 * @Date 2021/2/22 14:17
 * @Created by yanming.fu
 */
public class Mgr03 {

    private static Mgr03 INSTANCE;

    private Mgr03(){};

    public static Mgr03 getInstance(){
        if(INSTANCE == null){
            try {
                Thread.sleep(1);
            }catch (Exception e){
                e.printStackTrace();
            }
            INSTANCE=new Mgr03();
        }
        return INSTANCE;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                System.out.println(Mgr03.getInstance().hashCode());
            }).start();
        }
    }

}
