package com.ming.user.controller;

import cn.hutool.core.date.DateUtil;
import com.aliyun.oss.OSS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @Classname FileController
 * @Description 文件上传功能
 * @Date 2021/2/7 10:35
 * @Created by yanming.fu
 */
@RestController
@Api(tags = "文件上传")
public class FileController {

    @Resource
    private OSS ossClient;

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessId;

    @Value("${spring.cloud.alicloud.bucket-name}")
    private String bucketName;

    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endPoint;

    @ApiOperation(value = "上传文件")
    @PostMapping("/upload")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "上传的文件")
    })
     public String fileUpload(@RequestParam("file") MultipartFile file) throws IOException {


        String fileName = DateUtil.today().replaceAll("-", "/") + "/" + file.getOriginalFilename();
        ossClient.putObject(bucketName, fileName, file.getInputStream());
        //ossClient.shutdown();
        return "https://" + bucketName + "." + endPoint + "/" + fileName;
     }
}
