package com.ming.consumemq.processor;

import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import com.aliyun.openservices.ons.api.bean.Subscription;
import com.aliyun.openservices.shade.com.alibaba.rocketmq.common.filter.ExpressionType;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Classname ConsumerRegisterPostProcessor
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
public class ConsumerRegisterPostProcessor implements BeanDefinitionRegistryPostProcessor {

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(ConsumerBean.class);
        GenericBeanDefinition definition = (GenericBeanDefinition) builder.getRawBeanDefinition();

        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.AccessKey, "LTAI4Fu1YPhMMLdW6xRsCGvW");
        properties.setProperty(PropertyKeyConst.SecretKey, "3crHujqezmOpfL2dCBr670LaAXXyD0");
        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR, "http://MQ_INST_1015866164804423_BcED1bMI.cn-beijing.mq-internal.aliyuncs.com:8080");
        definition.getConstructorArgumentValues().addGenericArgumentValue(properties, Properties.class.getTypeName());

        Map<Subscription, MessageListener> subscriptionMap = new HashMap<>();
        Subscription ssc = new Subscription();
        ssc.setType(ExpressionType.TAG);
        ssc.setTopic("topic");
        ssc.setExpression("tagName");
        subscriptionMap.put(ssc, new DemoListener());
        Subscription ssc2 = new Subscription();
        ssc2.setType(ExpressionType.TAG);
        ssc2.setTopic("topic2");
        ssc2.setExpression("tagName2");
        subscriptionMap.put(ssc, new DemoListener());
        definition.getConstructorArgumentValues().addGenericArgumentValue(subscriptionMap, Map.class.getTypeName());

        definition.setInitMethodName("start");
        definition.setDestroyMethodName("shutdown");

        //definition.setAutowireMode(GenericBeanDefinition.AUTOWIRE_BY_TYPE);
        definition.setBeanClassName("com.aliyun.openservices.ons.api.bean.ConsumerBean");
        registry.registerBeanDefinition("consumerBean", definition);

    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
